//! Cassava Storage public traits
//!
//! In order to facilitate clients using any arbitrary backend for storage,
//! and to further ensure that no backend gives back concrete types (since that
//! would require some form of public use of the types) these traits exist.
//!
//! All of Cassava Storage APIs work by returning Box'd versions of these
//! traits.  The API is designed to be used in a Tokyo app and as such,
//! internally, Cassava objects have to be thread safe (using Arc, Mutex, etc).

use super::{RemovalResult, StorageChunk};
use futures::future::Future;
use futures::stream::Stream;
use std::path::{Path, PathBuf};

/// Cassava Storage Backend
///
/// All Cassava storage operations go via a storage backend which is constructed
/// by means of the `cassava_storage::backends` functions.
///
/// For example, you can get a basic memory backend as follows:
/// ```rust
/// # use cassava_storage::*;
/// let storage = backends::new_memory_backend();
/// // Use storage with the StorageBackendImpl trait
/// ```
///
/// All storage backends expose the same API via this trait.
pub trait StorageBackendImpl {
    /// Retrieve the current monotonic time in use by this backend.
    ///
    /// It is important to clients performing certain operations that they
    /// can know the current "time" as far as the backend is concerned.
    ///
    /// ```rust
    /// # use cassava_storage::*;
    /// # let storage = backends::new_memory_backend();
    /// println!("The current monotonic time is: {}", storage.get_monotonic_time());
    /// ```
    fn get_monotonic_time(&self) -> u64;

    /// Query whether or not a list of keys are present in the storage.
    ///
    /// For each key in the supplied vector of keys, a boolean will be returned
    /// which states whether *at the point of checking* the key was present in
    /// the storage.  It's important to note that this is not to be considered
    /// reliable when much time passes between the lookup and the response.
    ///
    /// When a key is looked up, it is marked as live, to reduce the chance that
    /// any ongoing garbage collection run will not delete it.
    ///
    /// ```rust
    /// # extern crate cassava_storage;
    /// # extern crate futures;
    /// # use cassava_storage::*;
    /// # use futures::prelude::*;
    /// # use std::path::{Path,PathBuf};
    /// # fn as_pathbuf(p:&str) -> PathBuf { let mut ret = PathBuf::new(); ret.push(p); ret }
    /// # let storage = backends::new_memory_backend();
    /// let desired = vec![as_pathbuf("first"), as_pathbuf("second")];
    /// let result = storage.check_presence(desired).wait().unwrap();
    /// # assert_eq!(result.len(), 2);
    /// # assert_eq!(result[0], false);
    /// # assert_eq!(result[1], false);
    /// ```
    fn check_presence(&self, keys: Vec<PathBuf>) -> Box<StoragePresenceChecker>;

    /// Add a new key/value pair (or update the value for a given key).
    ///
    /// The storage contains arbitrary key/value pairs (well UTF-8 keys)
    /// and so it's important for the storage to be able to be given new pairs.
    /// There is no intention to permit resumption of an upload via a different
    /// backend instance onto the same storage.  As such, this insertion process
    /// must be run to completion to guarantee a pair has been inserted.
    ///
    /// ```rust
    /// # extern crate cassava_storage;
    /// # extern crate futures;
    /// # use cassava_storage::*;
    /// # use futures::prelude::*;
    /// # use std::path::{Path,PathBuf};
    /// # fn as_pathbuf(p:&str) -> PathBuf { let mut ret = PathBuf::new(); ret.push(p); ret }
    /// # let storage = backends::new_memory_backend();
    /// let value = futures::stream::once(Ok(vec![1u8, 2u8]));
    /// let insertion = storage.insert_pair(as_pathbuf("key"), Box::new(value));
    /// let completer = insertion.wait().unwrap();
    /// completer.commit().wait().unwrap();
    /// # let checker = storage.check_presence(vec![as_pathbuf("key")]);
    /// # let result = checker.wait().unwrap();
    /// # assert_eq!(result.len(), 1);
    /// # assert_eq!(result[0], true);
    /// ```
    fn insert_pair(
        &self,
        key: PathBuf,
        value: Box<StorageValueStream>,
    ) -> Box<StorageInsertionHandler>;

    /// Retrieve a value from the backend storage.
    ///
    /// Once a key/value pair has been inserted into a storage, it can then be
    /// retrieved for use.  Retrieval marks the pair as being used recently
    /// in order to encourage garbage collection to retain the pair for longer.
    ///
    /// ```rust
    /// # extern crate cassava_storage;
    /// # extern crate futures;
    /// # use cassava_storage::*;
    /// # use futures::prelude::*;
    /// # use std::path::{Path,PathBuf};
    /// # fn as_pathbuf(p:&str) -> PathBuf { let mut ret = PathBuf::new(); ret.push(p); ret }
    /// # let storage = backends::new_memory_backend();
    /// # let value = futures::stream::once(Ok(vec![97u8, 98u8, 99u8, 100u8]));
    /// # let insertion = storage.insert_pair(as_pathbuf("key"), Box::new(value));
    /// # let completer = insertion.wait().unwrap();
    /// # completer.commit().wait().unwrap();
    /// let retriever = storage.retrieve_value("key".as_ref());
    /// let stream = retriever.wait().unwrap();
    /// let body = stream.collect().wait().unwrap();
    /// assert_eq!(body.len(), 1);
    /// let content = String::from_utf8(body[0].clone()).unwrap();
    /// assert_eq!(content, "abcd");
    /// ```
    fn retrieve_value(&self, key: &Path) -> Box<StorageRetrievalFuture>;

    /// Remove a key,value pair from the backend storage.
    ///
    /// From time to time, it may be necessary to remove a pair from a storage.
    /// In particular this will happen when the user of the storage attempts to
    /// garbage collect the store.  Since other users might have interacted with
    /// the storage in the time between deciding to remove an item and requesting
    /// the removal, the item might be looked at by another user of the storage.
    /// As such, it's required that a "minimum age" be passed in to remove a
    /// pair.  That is done by passing in a monotonic time value which the backend
    /// can use to decide whether to remove the pair or to refuse to.
    ///
    /// ```rust
    /// # extern crate cassava_storage;
    /// # extern crate futures;
    /// # use cassava_storage::*;
    /// # use futures::prelude::*;
    /// # use std::path::{Path,PathBuf};
    /// # fn as_pathbuf(p:&str) -> PathBuf { let mut ret = PathBuf::new(); ret.push(p); ret }
    /// # let storage = backends::new_memory_backend();
    /// # let value = futures::stream::once(Ok(vec![97u8, 98u8, 99u8, 100u8]));
    /// # let insertion = storage.insert_pair(as_pathbuf("key"), Box::new(value));
    /// # let completer = insertion.wait().unwrap();
    /// # completer.commit().wait().unwrap();
    /// let now_time = storage.get_monotonic_time();
    /// let result = storage.check_presence(vec![as_pathbuf("key")]).wait().unwrap();
    /// # assert_eq!(result.len(), 1);
    /// assert_eq!(result[0], true); // Checking the presence made the pair "newer"
    /// assert_eq!(storage.remove_pair("key".as_ref(), now_time)
    ///            .wait().unwrap(), RemovalResult::TooNew);
    /// // Trying again without looking at the pair
    /// let now_time = storage.get_monotonic_time();
    /// assert_eq!(storage.remove_pair("key".as_ref(), now_time)
    ///            .wait().unwrap(), RemovalResult::Removed);
    /// let result = storage.check_presence(vec![as_pathbuf("key")]).wait().unwrap();
    /// # assert_eq!(result.len(), 1);
    /// assert_eq!(result[0], false); // It's now not present
    /// ```
    fn remove_pair(&self, key: &Path, min_age: u64) -> Box<StorageRemovalFuture>;

    /// Enumerate keys which match the given prefix
    ///
    /// In order to support iterating the storage to determine what can be removed,
    /// the user of a storage is permitted to iterate keys which start with a
    /// given prefix.
    ///
    /// The pairs found during this enumeration will _not_ have their ages updated.
    ///
    /// ```rust
    /// # extern crate cassava_storage;
    /// # extern crate futures;
    /// # use cassava_storage::*;
    /// # use futures::prelude::*;
    /// # use std::path::{Path,PathBuf};
    /// # fn as_pathbuf(p:&str) -> PathBuf { let mut ret = PathBuf::new(); ret.push(p); ret }
    /// # let storage = backends::new_memory_backend();
    /// # let value = futures::stream::once(Ok(vec![97u8, 98u8, 99u8, 100u8]));
    /// # let insertion = storage.insert_pair(as_pathbuf("key/stored"), Box::new(value));
    /// # let completer = insertion.wait().unwrap();
    /// # completer.commit().wait().unwrap();
    /// let enumerator = storage.enumerate_keys("key".as_ref()).wait().unwrap();
    /// let all_keys = enumerator.collect().wait().unwrap();
    /// assert_eq!(all_keys, vec![as_pathbuf("key/stored")]);
    /// ```
    fn enumerate_keys(&self, prefix: &Path) -> Box<StorageEnumerationFuture>;
}

/// Backend super-type
///
/// Since many things require a storage backend to be both Send and Sync
/// this type is what is generally passed around
pub type StorageBackend = StorageBackendImpl + Send + Sync;

/// Cassava Storage Presence Checker
///
/// When checking for presence in a storage backend, the backend will return
/// a boxed struct which implements this trait.  The result from this future
/// is in the same order as the input vector consumed by `check_presence()`.
pub type StoragePresenceChecker = Future<Item = Vec<bool>, Error = ::std::io::Error> + Send;

/// Cassava Storage Value Stream
///
/// When inserting or retrieving values to/from a Storage, this stream is the
/// conduit through which content passes.
pub type StorageValueStream = Stream<Item = StorageChunk, Error = ::std::io::Error> + Send;

/// Cassava Storage Insertion Handler
///
/// When inserting a pair into the stream, this future will complete when all of
/// the storage chunks have been passed through the values stream and been
/// committed to the backend properly.  This won't be the final completion but
/// is the first step along the way.
pub type StorageInsertionHandler =
    Future<Item = Box<StorageInsertionCompletion>, Error = ::std::io::Error> + Send;

/// Cassava Storage Insertion completer
///
/// When inserting a pair into the stream, after the content has been stored,
/// there is a time when the client can make a decision to commit or roll back
/// the storage attempt.
pub trait StorageInsertionCompletion: Send {
    /// Retrieve the key under which the pair will be stored.
    fn key(&self) -> &Path;
    /// Retrieve a stream which is the content of the pair to be stored.
    fn value(&self) -> Box<StorageRetrievalFuture>;
    /// Choose to commit this pair to the storage
    ///
    /// This consumes the boxed insertion completer so the client cannot call
    /// it more than once.
    fn commit(self: Box<Self>) -> Box<StorageInsertionResult>;
    /// Choose to abort this pair and not store it
    ///
    /// This consumes the boxed insertion completer so the client cannot call
    /// it more than once.
    fn abort(self: Box<Self>) -> Box<StorageInsertionResult>;
}

/// When committing or aborting an insertion, this is the future you get
pub type StorageInsertionResult = Future<Item = (), Error = ::std::io::Error> + Send;

/// When retrieving from a storage, this is the future you get
pub type StorageRetrievalFuture =
    Future<Item = Box<StorageValueStream>, Error = ::std::io::Error> + Send;

/// When removing a pair from storage, this is the future you get
pub type StorageRemovalFuture = Future<Item = RemovalResult, Error = ::std::io::Error> + Send;

/// When enumerating keys in a storage, this is the future you get.
///
/// The future resolves to a boxed stream of keys which match the prefix.
pub type StorageEnumerationFuture =
    Future<Item = Box<StorageEnumerationStream>, Error = ::std::io::Error> + Send;

/// When a storage enumeration has begun, this stream will yield matching keys.
pub type StorageEnumerationStream = Stream<Item = PathBuf, Error = ::std::io::Error> + Send;
