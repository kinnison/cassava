//! Utility functions and types for Cassava Storage

use futures::stream::Stream;
use futures::{Async, Poll};

use super::{StorageChunk, StorageValueStream};

pub struct TrivialMemoryStream {
    chunks: Vec<StorageChunk>,
}

impl TrivialMemoryStream {
    pub fn from_chunks(chunks: Vec<StorageChunk>) -> Box<StorageValueStream> {
        Box::new(TrivialMemoryStream {
            chunks: chunks.into_iter().rev().collect(),
        })
    }
}

impl Stream for TrivialMemoryStream {
    type Item = StorageChunk;
    type Error = ::std::io::Error;

    fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
        if !self.chunks.is_empty() {
            let ret = self.chunks.pop();
            Ok(Async::Ready(ret))
        } else {
            Ok(Async::Ready(None))
        }
    }
}
