//! Backends in Cassava Storage
//!
mod fs;
mod memory;

use super::traits::StorageBackend;

use std::io;

/// Create a new memory based backend
///
/// The memory backends are not shared among themselves, each is a unique pool
/// of storage.  The backend is not optimised and is not meant for production.
/// Instead this backend is expected to be used for basic testing, both of
/// the storage system itself and of anything using a storage.
pub fn new_memory_backend() -> Box<StorageBackend> {
    Box::new(memory::MemoryBackend::new())
}

#[doc(inline)]
pub use self::fs::FilesystemBackendConfig;

/// Create a new filesystem based backend
///
/// The filesystem backends share content by means of a POSIX filesystem with
/// atomic `mkdir()` properties.  Almost any POSIX filesystem will do, including
/// NFS.
pub fn new_filesystem_backend(
    config: FilesystemBackendConfig,
) -> Result<Box<StorageBackend>, io::Error> {
    Ok(Box::new(fs::FilesystemBackend::try_new(config)?))
}
