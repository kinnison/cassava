//! Memory backed storage for Cassava
//!
//! This is the memory backed storage for Cassava

use super::super::*;

use futures::future::Future;
use futures::task;
use futures::Stream;
use futures::{Async, Poll};
use std::collections::{hash_map::Entry, HashMap};
use std::path::{Path, PathBuf};
use std::sync::{Arc, Mutex};

struct MemoryBackendContentStruct {
    chunks: Vec<StorageChunk>,
    age: u64,
}

type MemoryBackendContent = Arc<Mutex<MemoryBackendContentStruct>>;

struct MemoryBackendInnerStruct {
    used: usize,
    content: HashMap<PathBuf, MemoryBackendContent>,
    monotime: u64,
}

type MemoryBackendInner = Arc<Mutex<MemoryBackendInnerStruct>>;

/// A Memory based Cassava StorageBackend
///
/// This backend is not optimised nor is it meant for complex production
/// use.  It's typically meant for use in test code.
pub struct MemoryBackend {
    inner: MemoryBackendInner,
}

struct MemoryBackendPresenceChecker {
    inner: MemoryBackendInner,
    to_check: Vec<PathBuf>,
    results: Vec<bool>,
}

struct MemoryBackendInsertionHandler {
    inner: MemoryBackendInner,
    key: PathBuf,
    stream: Box<Future<Item = Vec<StorageChunk>, Error = ::std::io::Error> + Send>,
}

struct MemoryBackendInsertionCompleter {
    inner: MemoryBackendInner,
    key: PathBuf,
    content: MemoryBackendContent,
}

struct MemoryBackendValueStreamer {
    content: MemoryBackendContent,
    index: usize,
}

impl MemoryBackend {
    /// Create a new memory backend.
    ///
    /// This backend will be empty and can be shared and sent among threads.
    pub fn new() -> MemoryBackend {
        let inner = MemoryBackendInnerStruct {
            used: 0,
            content: HashMap::new(),
            monotime: 0,
        };
        MemoryBackend {
            inner: Arc::new(Mutex::new(inner)),
        }
    }
}

impl StorageBackendImpl for MemoryBackend {
    fn get_monotonic_time(&self) -> u64 {
        self.inner.lock().unwrap().monotime
    }

    fn check_presence(&self, keys: Vec<PathBuf>) -> Box<StoragePresenceChecker> {
        let results = Vec::with_capacity(keys.len());
        let ret = MemoryBackendPresenceChecker {
            inner: self.inner.clone(),
            to_check: keys.into_iter().rev().collect(),
            results,
        };
        Box::new(ret)
    }

    fn insert_pair(
        &self,
        key: PathBuf,
        value: Box<StorageValueStream>,
    ) -> Box<StorageInsertionHandler> {
        let collect = Box::new(value.collect());
        Box::new(MemoryBackendInsertionHandler {
            inner: self.inner.clone(),
            key,
            stream: collect,
        })
    }

    fn retrieve_value(&self, key: &Path) -> Box<StorageRetrievalFuture> {
        // We can retrieve instantly so let's be super-fast about this...
        let mut inner = self.inner.lock().unwrap();
        inner.monotime += 1;
        if inner.content.contains_key(key) {
            let streamer: Box<StorageValueStream> = Box::new(MemoryBackendValueStreamer {
                index: 0,
                content: inner.content[key].clone(),
            });
            Box::new(futures::future::ok(streamer))
        } else {
            Box::new(futures::future::err(::std::io::Error::from(
                ::std::io::ErrorKind::NotFound,
            )))
        }
    }

    fn remove_pair(&self, key: &Path, min_age: u64) -> Box<StorageRemovalFuture> {
        let mut inner = self.inner.lock().unwrap();
        inner.monotime += 1;
        if inner.content.contains_key(key) {
            let age = {
                let content = inner.content[key].lock().unwrap();
                content.age
            };
            if age < min_age {
                inner.content.remove(key);
                Box::new(futures::future::ok(RemovalResult::Removed))
            } else {
                Box::new(futures::future::ok(RemovalResult::TooNew))
            }
        } else {
            Box::new(futures::future::err(::std::io::Error::from(
                ::std::io::ErrorKind::NotFound,
            )))
        }
    }

    fn enumerate_keys(&self, prefix: &Path) -> Box<StorageEnumerationFuture> {
        let keys: Vec<PathBuf> = self
            .inner
            .lock()
            .unwrap()
            .content
            .keys()
            .filter(|k| k.starts_with(prefix))
            .map(|k| k.to_owned())
            .collect();
        let stream: Box<Stream<Item = PathBuf, Error = ::std::io::Error> + Send> =
            Box::new(futures::stream::iter_ok(keys));
        Box::new(futures::future::ok(stream))
    }
}

impl Future for MemoryBackendPresenceChecker {
    type Item = Vec<bool>;
    type Error = ::std::io::Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        // We're deliberately designed here to be slow and require many polls
        if !self.to_check.is_empty() {
            let mut inner = self.inner.lock().unwrap();
            let key = self.to_check.pop().unwrap();
            let nowtime = inner.monotime;
            let mut entry = inner.content.entry(key);
            let present = match entry {
                Entry::Occupied(ref mut oe) => {
                    oe.get_mut().lock().unwrap().age = nowtime;
                    true
                }
                Entry::Vacant(_) => false,
            };
            self.results.push(present);
            task::current().notify();
            Ok(Async::NotReady)
        } else {
            Ok(Async::Ready(self.results.clone()))
        }
    }
}

impl Future for MemoryBackendInsertionHandler {
    type Item = Box<StorageInsertionCompletion>;
    type Error = ::std::io::Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        let chunks = try_ready!(self.stream.poll());
        Ok(Async::Ready(Box::new(MemoryBackendInsertionCompleter {
            inner: self.inner.clone(),
            key: self.key.clone(),
            content: Arc::new(Mutex::new(MemoryBackendContentStruct {
                chunks,
                age: self.inner.lock().unwrap().monotime,
            })),
        })))
    }
}

impl StorageInsertionCompletion for MemoryBackendInsertionCompleter {
    fn key(&self) -> &Path {
        self.key.as_ref()
    }

    fn value(&self) -> Box<StorageRetrievalFuture> {
        let boxed: Box<StorageValueStream> = Box::new(MemoryBackendValueStreamer {
            content: self.content.clone(),
            index: 0,
        });
        Box::new(futures::future::ok(boxed))
    }

    #[allow(unknown_lints, clippy::boxed_local)]
    fn commit(self: Box<Self>) -> Box<StorageInsertionResult> {
        let mut memback = self.inner.lock().unwrap();
        memback.monotime += 1;
        self.content.lock().unwrap().age = memback.monotime;
        memback.used += self.key.as_os_str().len();
        memback.used += self.content.lock().unwrap().size();
        memback
            .content
            .insert(self.key.clone(), self.content.clone());
        Box::new(futures::future::ok(()))
    }

    #[allow(unknown_lints, clippy::boxed_local)]
    fn abort(self: Box<Self>) -> Box<StorageInsertionResult> {
        Box::new(futures::future::ok(()))
    }
}

impl Stream for MemoryBackendValueStreamer {
    type Item = StorageChunk;
    type Error = ::std::io::Error;

    fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
        let chunks = self.content.lock().unwrap();
        if self.index < chunks.chunks.len() {
            // Unfortunate copy, but most backends won't have saved this so we
            // pretend we retrieved it from somewhere and passed ownership to
            // the caller.
            let ret = chunks.chunks[self.index].clone();
            self.index += 1;
            Ok(Async::Ready(Some(ret)))
        } else {
            Ok(Async::Ready(None))
        }
    }
}

impl MemoryBackendContentStruct {
    fn size(&self) -> usize {
        self.chunks.iter().map(|v| v.len()).sum()
    }
}
