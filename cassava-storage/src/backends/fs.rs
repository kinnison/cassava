//! Filesystem backend for Cassava Storage

use crate::RemovalResult;
use futures;
use futures::{Async, Future, Poll, Stream};

use tokio_fs;
use tokio_io::{AsyncRead, AsyncWrite};
use tokio_timer;

use sha2::{Digest, Sha256};

use hex;
use tempfile::NamedTempFile;

use walkdir;

use std::io;
use std::path::{Path, PathBuf};
use std::sync::Arc;
use std::time;

use crate::traits::*;

/// Configuration for a filesystem backend of Cassava Storage.
///
/// A filesystem backend needs quite a bit of configuration in
/// order to work properly.  This encapsulates the configuration
/// and permits the user to manage how the backend behaves.  It's
/// important that all clients talking to the same filesystem backend
/// should have the same configuration or else undefined behaviour may
/// ensue.
///
/// _Times are always in seconds_
pub struct FilesystemBackendConfig {
    /// The directory that the backend will use.
    ///
    /// This will automatically gain a `content`, `tmp`, and `locks` set of
    /// subdirectories.
    pub base_path: PathBuf,
    /// The maximum age in seconds that an object can be before locking has to
    /// happen when updating the age.
    pub max_unlocked: u64,
    /// The minimum age in seconds that an object must be before it gets removed
    /// automatically by garbage collection.
    pub min_removal: u64,
    /// The maximum age for a lock before it's considered stale
    pub max_locked: u64,
    /// The number of times a lock is attempted before giving up
    pub lock_maxtries: usize,
    /// The time between lock attempts
    pub lock_trygap: u64,
}

impl FilesystemBackendConfig {
    fn content_path<P>(&self, key: P) -> PathBuf
    where
        P: AsRef<Path>,
    {
        let mut ret = self.base_path.clone();
        ret.push("content");
        ret.push(key);
        ret
    }

    fn lock_path<P>(&self, key: P) -> PathBuf
    where
        P: AsRef<Path>,
    {
        use std::os::unix::ffi::OsStrExt;
        let mut ret = self.base_path.clone();
        ret.push("locks");
        ret.push(hex::encode(Sha256::digest(
            key.as_ref().as_os_str().as_bytes(),
        )));
        ret
    }

    fn tmp_path(&self) -> PathBuf {
        let mut ret = self.base_path.clone();
        ret.push("tmp");
        ret
    }
}

fn msecs_since(earlier: time::SystemTime) -> u64 {
    if let Ok(delta) = time::SystemTime::now().duration_since(earlier) {
        (delta.as_secs() * 1000) + u64::from(delta.subsec_millis())
    } else {
        0
    }
}

pub struct FilesystemBackend {
    conf: Arc<FilesystemBackendConfig>,
}

enum FSBKRMachine {
    Start,
    FileBasedAcquire(
        Box<
            Future<Item = <tokio_fs::file::MetadataFuture as Future>::Item, Error = io::Error>
                + Send,
        >,
    ),
    TryLock(Box<Future<Item = Option<tokio_fs::file::File>, Error = io::Error> + Send>),
    Success(Box<Future<Item = tokio_fs::file::File, Error = io::Error> + Send>),
    Missing(Box<Future<Item = (), Error = io::Error> + Send>),
    Retry(Box<Future<Item = (), Error = io::Error> + Send>),
}

struct FSBKeyRetriever {
    conf: Arc<FilesystemBackendConfig>,
    key: PathBuf,
    tries_left: usize,
    machine: FSBKRMachine,
}

impl FSBKeyRetriever {
    fn new(conf: Arc<FilesystemBackendConfig>, key: PathBuf) -> FSBKeyRetriever {
        let max_tries = conf.lock_maxtries;
        FSBKeyRetriever {
            conf,
            key,
            tries_left: max_tries,
            machine: FSBKRMachine::Start,
        }
    }
}

impl Future for FSBKeyRetriever {
    type Item = Option<tokio_fs::file::File>;
    type Error = io::Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        loop {
            let mut newstate = FSBKRMachine::Start;
            match self.machine {
                FSBKRMachine::Start => {
                    newstate = FSBKRMachine::FileBasedAcquire(Box::new(
                        tokio_fs::file::File::open(self.conf.content_path(&self.key))
                            .and_then(|f| f.metadata()),
                    ))
                }
                FSBKRMachine::FileBasedAcquire(ref mut fut) => {
                    let res = fut.poll();
                    let (fh, meta) = match res {
                        Ok(Async::Ready((fh, meta))) => (fh, meta),
                        Ok(Async::NotReady) => {
                            return Ok(Async::NotReady);
                        }
                        Err(e) => {
                            if e.kind() == io::ErrorKind::NotFound {
                                return Ok(Async::Ready(None));
                            }
                            return Err(e);
                        }
                    };
                    if let Ok(systime) = meta.modified() {
                        if msecs_since(systime) < self.conf.max_unlocked {
                            let mut fh = Some(fh);
                            newstate = FSBKRMachine::Success(Box::new(futures::future::poll_fn(
                                move || {
                                    let newfh = match fh.as_mut().unwrap().poll_try_clone() {
                                        Ok(Async::NotReady) => return Ok(Async::NotReady),
                                        Ok(Async::Ready(fh)) => fh.into_std(),
                                        Err(e) => return Err(e),
                                    };
                                    blocking::io(|| {
                                        let ret = platform::set_utime_now(&newfh);
                                        ret.map(|_| fh.take().unwrap())
                                    })
                                },
                            )))
                        } else if self.tries_left > 0 {
                            // We need to try and lock the file before updating
                            self.tries_left -= 1;

                            newstate = FSBKRMachine::TryLock(Box::new(
                                tokio_fs::create_dir(self.conf.lock_path(&self.key)).then(|r| {
                                    match r {
                                        Ok(()) => Ok(Some(fh)),
                                        Err(e) => {
                                            if e.kind() == io::ErrorKind::AlreadyExists {
                                                Ok(None)
                                            } else {
                                                Err(e)
                                            }
                                        }
                                    }
                                }),
                            ));
                        } else {
                            // We need to close the file (ideally in a blocking block)
                            // and then return missing. (since we can't lock)
                            let mut fh = Some(fh.into_std());
                            newstate = FSBKRMachine::Missing(Box::new(futures::future::poll_fn(
                                move || {
                                    blocking::io(|| {
                                        fh.take();
                                        Ok(())
                                    })
                                },
                            )))
                        }
                    } else {
                        // Unable to retrieve the system time, so give up
                        return Err(io::Error::from(io::ErrorKind::InvalidData));
                    }
                }
                FSBKRMachine::Success(ref mut fut) => {
                    let fh = try_ready!(fut.poll());
                    return Ok(Async::Ready(Some(fh)));
                }
                FSBKRMachine::Missing(ref mut fut) => {
                    try_ready!(fut.poll());
                    return Ok(Async::Ready(None));
                }
                FSBKRMachine::TryLock(ref mut fut) => {
                    let res = match fut.poll() {
                        Ok(Async::NotReady) => return Ok(Async::NotReady),
                        Ok(Async::Ready(res)) => res,
                        Err(e) => return Err(e),
                    };
                    if let Some(fh) = res {
                        // Acquired lock, update FH, unlock, return
                        let mut fh = Some(fh);
                        let fut = futures::future::poll_fn(move || {
                            let newfh = match fh.as_mut().unwrap().poll_try_clone() {
                                Ok(Async::NotReady) => return Ok(Async::NotReady),
                                Ok(Async::Ready(fh)) => fh.into_std(),
                                Err(e) => return Err(e),
                            };
                            try_ready!(blocking::io(move || platform::set_utime_now(&newfh)));
                            Ok(Async::Ready(fh.take().unwrap()))
                        });
                        let mut dirremover = tokio_fs::remove_dir(self.conf.lock_path(&self.key));
                        let remover = move |passed| dirremover.poll().map(|_| passed);
                        newstate = FSBKRMachine::Success(Box::new(fut.and_then(remover)))
                    } else {
                        // Failed to acquire lock, TODO: Wait and retry
                        let timer =
                            tokio_timer::sleep(time::Duration::from_millis(self.conf.lock_trygap));
                        newstate = FSBKRMachine::Retry(Box::new(
                            timer.map_err(|_| io::Error::from(io::ErrorKind::TimedOut)),
                        ));
                    }
                }
                FSBKRMachine::Retry(ref mut fut) => {
                    try_ready!(fut.poll());
                    // Nothing to do if we have completed, since we want to go back
                    // to the start state
                }
            }
            self.machine = newstate;
        }
    }
}

struct FSBIndividualPresenceChecker {
    inner: FSBKeyRetriever,
    fh: Option<tokio_fs::file::File>,
}

impl FSBIndividualPresenceChecker {
    fn new(conf: Arc<FilesystemBackendConfig>, key: PathBuf) -> FSBIndividualPresenceChecker {
        FSBIndividualPresenceChecker {
            inner: FSBKeyRetriever::new(conf, key),
            fh: None,
        }
    }
}

impl Future for FSBIndividualPresenceChecker {
    type Item = bool;
    type Error = io::Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        if self.fh.is_none() {
            self.fh = try_ready!(self.inner.poll());
            if self.fh.is_none() {
                return Ok(Async::Ready(false));
            }
        }
        try_ready!(self.fh.as_mut().unwrap().shutdown());
        Ok(Async::Ready(true))
    }
}

struct FilesystemBackendPresenceChecker {
    conf: Arc<FilesystemBackendConfig>,
    in_progress: Option<FSBIndividualPresenceChecker>,
    tocheck: Vec<PathBuf>,
    result: Vec<bool>,
}

struct FilesystemBackendFileStreamer {
    fh: Option<tokio_fs::file::File>,
    stdfh: Option<::std::fs::File>,
    chunk: Vec<u8>,
}

impl Stream for FilesystemBackendFileStreamer {
    type Item = Vec<u8>;
    type Error = io::Error;

    fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
        let count = try_ready!(self.fh.as_mut().unwrap().read_buf(&mut self.chunk));
        if count == 0 {
            // End of file, presumably
            try_ready!(blocking::io(|| {
                ::std::mem::replace(&mut self.stdfh, None);
                ::std::mem::replace(&mut self.fh, None);
                Ok(())
            }));
            Ok(Async::Ready(None))
        } else {
            platform::set_utime_now(self.stdfh.as_ref().unwrap())?;
            Ok(Async::Ready(Some(::std::mem::replace(
                &mut self.chunk,
                Vec::with_capacity(1024 * 1024),
            ))))
        }
    }
}

impl FilesystemBackendFileStreamer {
    fn from_file(
        fh: tokio_fs::file::File,
    ) -> Box<Future<Item = Box<StorageValueStream>, Error = io::Error> + Send> {
        let mut holder = Some(fh);
        Box::new(futures::future::poll_fn(move || {
            let fh2 = try_ready!(holder.as_mut().unwrap().poll_try_clone());
            let boxed: Box<StorageValueStream> = Box::new(FilesystemBackendFileStreamer {
                fh: holder.take(),
                stdfh: Some(fh2.into_std()),
                chunk: Vec::with_capacity(1024 * 1024),
            });
            Ok(Async::Ready(boxed))
        }))
    }
}

enum FSBIHMachine {
    Start,
    Opening(Box<Future<Item = NamedTempFile, Error = io::Error> + Send>),
    Acquiring(Option<NamedTempFile>),
    Storing(Option<NamedTempFile>, Option<crate::StorageChunk>),
    Finishing(Option<FSBInsertionCompletion>),
}

struct FSBInsertionHandler {
    conf: Arc<FilesystemBackendConfig>,
    key: Option<PathBuf>,
    value: Box<StorageValueStream>,
    state: FSBIHMachine,
}

struct FSBInsertionCompletion {
    conf: Arc<FilesystemBackendConfig>,
    tempfile: NamedTempFile,
    key: PathBuf,
}

impl FSBInsertionHandler {
    fn new(
        conf: Arc<FilesystemBackendConfig>,
        key: PathBuf,
        value: Box<StorageValueStream>,
    ) -> FSBInsertionHandler {
        FSBInsertionHandler {
            conf,
            key: Some(key),
            value,
            state: FSBIHMachine::Start,
        }
    }
}

impl Future for FSBInsertionHandler {
    type Item = Box<StorageInsertionCompletion>;
    type Error = io::Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        loop {
            self.state = match self.state {
                FSBIHMachine::Start => {
                    let temppath = self.conf.tmp_path();
                    let fut = futures::future::poll_fn(move || {
                        blocking::io(|| NamedTempFile::new_in(&temppath))
                    });
                    FSBIHMachine::Opening(Box::new(fut))
                }
                FSBIHMachine::Opening(ref mut fut) => {
                    let tempfile = try_ready!(fut.poll());
                    FSBIHMachine::Acquiring(Some(tempfile))
                }
                FSBIHMachine::Acquiring(ref mut file) => {
                    let chunk = try_ready!(self.value.poll());
                    if let Some(chunk) = chunk {
                        FSBIHMachine::Storing(file.take(), Some(chunk))
                    } else {
                        FSBIHMachine::Finishing(Some(FSBInsertionCompletion {
                            conf: self.conf.clone(),
                            tempfile: file.take().unwrap(),
                            key: self.key.take().unwrap(),
                        }))
                    }
                }
                FSBIHMachine::Storing(ref mut file, ref mut chunk) => {
                    use std::io::Write;
                    let written = try_ready!(blocking::io(|| file
                        .as_mut()
                        .unwrap()
                        .write(chunk.as_ref().unwrap())));
                    if written == chunk.as_ref().unwrap().len() {
                        FSBIHMachine::Acquiring(file.take())
                    } else {
                        {
                            let vec = chunk.as_mut().unwrap();
                            let newlen = vec.len() - written;
                            vec.rotate_left(written);
                            vec.truncate(newlen);
                        }
                        FSBIHMachine::Storing(file.take(), chunk.take())
                    }
                }
                FSBIHMachine::Finishing(ref mut result) => {
                    let ret = result.take().unwrap();
                    let boxed: Box<StorageInsertionCompletion> = Box::new(ret);
                    return Ok(Async::Ready(boxed));
                }
            };
        }
    }
}

enum FSBICMachine {
    Start,
    Acquiring(Box<Future<Item = bool, Error = io::Error> + Send>),
    Retrying(Box<Future<Item = (), Error = io::Error> + Send>),
    Moving(Box<Future<Item = (), Error = io::Error> + Send>),
    Unlocking(Box<Future<Item = (), Error = io::Error> + Send>),
    Abandoning(Box<Future<Item = (), Error = io::Error> + Send>),
}

struct FSBInsertionCommitter {
    conf: Arc<FilesystemBackendConfig>,
    key: PathBuf,
    tempfile: Option<NamedTempFile>,
    state: FSBICMachine,
    triesleft: usize,
}

impl Future for FSBInsertionCommitter {
    type Item = ();
    type Error = io::Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        loop {
            self.state = match self.state {
                FSBICMachine::Start => {
                    let fut =
                        tokio_fs::create_dir(self.conf.lock_path(&self.key)).then(|r| match r {
                            Ok(()) => Ok(true),
                            Err(e) => {
                                if e.kind() == io::ErrorKind::AlreadyExists {
                                    Ok(false)
                                } else {
                                    Err(e)
                                }
                            }
                        });
                    FSBICMachine::Acquiring(Box::new(fut))
                }
                FSBICMachine::Acquiring(ref mut fut) => {
                    let res = try_ready!(fut.poll());
                    if res {
                        // Succeeded in acquiring the lock
                        let mut tf = self.tempfile.take();
                        let target = self.conf.content_path(&self.key);
                        let makedir = target
                            .parent()
                            .expect("Unable to get parent? Oddness!")
                            .to_path_buf();
                        let maker = tokio_fs::create_dir_all(makedir);
                        let fut = futures::future::poll_fn(move || {
                            blocking::io(|| {
                                if let Some(tempfile) = tf.take() {
                                    let res = tempfile.persist(&target);
                                    // Note: Doing this means we'll drop the
                                    // tempfile during this future
                                    res.map(|_| ()).map_err(|e| e.error)
                                } else {
                                    Err(io::Error::from(io::ErrorKind::Other))
                                }
                            })
                        });
                        FSBICMachine::Moving(Box::new(maker.and_then(|_| fut)))
                    } else {
                        // Failed to acquire the lock, retry maybe?
                        let timer =
                            tokio_timer::sleep(time::Duration::from_millis(self.conf.lock_trygap));
                        FSBICMachine::Retrying(Box::new(
                            timer.map_err(|_| io::Error::from(io::ErrorKind::TimedOut)),
                        ))
                    }
                }
                FSBICMachine::Retrying(ref mut fut) => {
                    try_ready!(fut.poll());
                    if self.triesleft > 0 {
                        self.triesleft -= 1;
                        FSBICMachine::Start
                    } else {
                        // No tries left, we need to clean up and return failure
                        let mut tf = self.tempfile.take();
                        let fut = futures::future::poll_fn(move || {
                            blocking::io(|| {
                                tf.take();
                                Ok(())
                            })
                        });
                        FSBICMachine::Abandoning(Box::new(fut))
                    }
                }
                FSBICMachine::Abandoning(ref mut fut) => {
                    try_ready!(fut.poll());
                    return Err(io::Error::from(io::ErrorKind::TimedOut));
                }
                FSBICMachine::Moving(ref mut fut) => {
                    let res = match fut.poll() {
                        Ok(Async::NotReady) => return Ok(Async::NotReady),
                        Err(e) => Err(e),
                        Ok(Async::Ready(())) => Ok(()),
                    };
                    let dirremover = tokio_fs::remove_dir(self.conf.lock_path(&self.key));
                    if res.is_ok() {
                        // Successfully put in place, we need to unlock and
                        // return success
                        FSBICMachine::Unlocking(Box::new(dirremover))
                    } else {
                        // Failed to put in place, we need to unlock and then
                        // deliberately raise the error we had from the move
                        FSBICMachine::Unlocking(Box::new(dirremover.and_then(|_| res)))
                    }
                }
                FSBICMachine::Unlocking(ref mut fut) => {
                    try_ready!(fut.poll());
                    return Ok(Async::Ready(()));
                }
            };
        }
    }
}

impl StorageInsertionCompletion for FSBInsertionCompletion {
    fn key(&self) -> &Path {
        self.key.as_path()
    }

    fn value(&self) -> Box<StorageRetrievalFuture> {
        let path = self.tempfile.path().to_path_buf();
        let fut =
            tokio_fs::file::File::open(path).and_then(FilesystemBackendFileStreamer::from_file);
        Box::new(fut)
    }

    #[allow(unknown_lints, clippy::boxed_local)]
    fn commit(self: Box<Self>) -> Box<StorageInsertionResult> {
        let ic = *self;
        let tries = ic.conf.lock_maxtries;
        Box::new(FSBInsertionCommitter {
            conf: ic.conf,
            key: ic.key,
            tempfile: Some(ic.tempfile),
            state: FSBICMachine::Start,
            triesleft: tries,
        })
    }

    #[allow(unknown_lints, clippy::boxed_local)]
    fn abort(self: Box<Self>) -> Box<StorageInsertionResult> {
        // Since we essentially have nothing to do, we just want to ensure that
        // our file handle is dropped/cleaned up inside a future...
        let mut tf = Some(self.tempfile);
        Box::new(futures::future::poll_fn(move || {
            blocking::io(|| {
                tf.take();
                Ok(())
            })
        }))
    }
}

enum FSBRMachine {
    Start,
    Acquiring(Box<Future<Item = bool, Error = io::Error> + Send>),
    Retrying(Box<Future<Item = (), Error = io::Error> + Send>),
    Checking(Box<Future<Item = bool, Error = io::Error> + Send>),
    Removing(Box<Future<Item = (), Error = io::Error> + Send>),
    DoUnlock,
    Unlocking(Box<Future<Item = (), Error = io::Error> + Send>),
}

struct FSBPairRemover {
    conf: Arc<FilesystemBackendConfig>,
    key: PathBuf,
    min_age: u64,
    state: FSBRMachine,
    result: Option<io::Result<RemovalResult>>,
    triesleft: usize,
}

impl Future for FSBPairRemover {
    type Item = RemovalResult;
    type Error = io::Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        loop {
            self.state = match self.state {
                FSBRMachine::Start => {
                    let fut =
                        tokio_fs::create_dir(self.conf.lock_path(&self.key)).then(|r| match r {
                            Ok(()) => Ok(true),
                            Err(e) => {
                                if e.kind() == io::ErrorKind::AlreadyExists {
                                    Ok(false)
                                } else {
                                    Err(e)
                                }
                            }
                        });
                    FSBRMachine::Acquiring(Box::new(fut))
                }
                FSBRMachine::Acquiring(ref mut fut) => {
                    if try_ready!(fut.poll()) {
                        // Acquired the lock, so maybe remove the file?
                        let min_age = self.min_age;
                        FSBRMachine::Checking(Box::new(
                            tokio_fs::metadata(self.conf.content_path(&self.key)).and_then(
                                move |meta| {
                                    let age =
                                        meta.modified()?.duration_since(time::UNIX_EPOCH).unwrap();
                                    Ok(age <= time::Duration::from_millis(min_age))
                                },
                            ),
                        ))
                    } else {
                        // Failed to acquire the lock, retry maybe?
                        let timer =
                            tokio_timer::sleep(time::Duration::from_millis(self.conf.lock_trygap));
                        FSBRMachine::Retrying(Box::new(
                            timer.map_err(|_| io::Error::from(io::ErrorKind::TimedOut)),
                        ))
                    }
                }
                FSBRMachine::Retrying(ref mut fut) => {
                    try_ready!(fut.poll());
                    if self.triesleft > 0 {
                        self.triesleft -= 1;
                        FSBRMachine::Start
                    } else {
                        // No tries left, we need to clean up and return failure
                        return Err(io::Error::from(io::ErrorKind::TimedOut));
                    }
                }
                FSBRMachine::Checking(ref mut fut) => match fut.poll() {
                    Err(e) => {
                        self.result = Some(Err(e));
                        FSBRMachine::DoUnlock
                    }
                    Ok(Async::NotReady) => return Ok(Async::NotReady),
                    Ok(Async::Ready(res)) => {
                        if res {
                            // We should try and remove
                            let remover = tokio_fs::remove_file(self.conf.content_path(&self.key));
                            self.result = Some(Ok(RemovalResult::Removed));
                            FSBRMachine::Removing(Box::new(remover))
                        } else {
                            // We don't remove, it's too new
                            self.result = Some(Ok(RemovalResult::TooNew));
                            FSBRMachine::DoUnlock
                        }
                    }
                },
                FSBRMachine::Removing(ref mut fut) => {
                    try_ready!(fut.poll());
                    FSBRMachine::DoUnlock
                }
                FSBRMachine::DoUnlock => {
                    let remover = tokio_fs::remove_dir(self.conf.lock_path(&self.key));
                    FSBRMachine::Unlocking(Box::new(remover))
                }
                FSBRMachine::Unlocking(ref mut fut) => {
                    try_ready!(fut.poll());
                    return match self.result.take() {
                        None => Err(io::Error::from(io::ErrorKind::InvalidData)),
                        Some(Err(e)) => Err(e),
                        Some(Ok(res)) => Ok(Async::Ready(res)),
                    };
                }
            }
        }
    }
}

enum FSBEMachine {
    Start,
    Creating(Box<Future<Item = walkdir::IntoIter, Error = io::Error> + Send>),
    PrepFetch(Option<walkdir::IntoIter>),
    Fetching(Box<Future<Item = (walkdir::IntoIter, Option<PathBuf>), Error = io::Error> + Send>),
    Ending(Box<Future<Item = (), Error = io::Error> + Send>),
}

struct FSBEnumerator {
    conf: Arc<FilesystemBackendConfig>,
    prefix: PathBuf,
    state: FSBEMachine,
    nextitem: Option<PathBuf>,
}

impl FSBEnumerator {
    fn new(conf: Arc<FilesystemBackendConfig>, prefix: &Path) -> FSBEnumerator {
        FSBEnumerator {
            conf,
            prefix: prefix.to_path_buf(),
            state: FSBEMachine::Start,
            nextitem: None,
        }
    }
}

impl Stream for FSBEnumerator {
    type Item = PathBuf;
    type Error = io::Error;

    fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
        loop {
            self.state = match self.state {
                FSBEMachine::Start => {
                    let base = self.conf.content_path(&self.prefix);
                    FSBEMachine::Creating(Box::new(futures::future::poll_fn(move || {
                        blocking::io(|| {
                            Ok(walkdir::WalkDir::new(&base).follow_links(false).into_iter())
                        })
                    })))
                }
                FSBEMachine::Creating(ref mut fut) => {
                    let mut iter = try_ready!(fut.poll());
                    FSBEMachine::PrepFetch(Some(iter))
                }
                FSBEMachine::PrepFetch(ref mut iterholder) => {
                    let mut iter = iterholder.take();
                    FSBEMachine::Fetching(Box::new(futures::future::poll_fn(move || {
                        let mut iter = iter.take().unwrap();
                        blocking::io(|| loop {
                            let res = match iter.next() {
                                None => Ok(Async::Ready(None)),
                                Some(Err(e)) => Err(e),
                                Some(Ok(entry)) => match entry.metadata() {
                                    Err(e) => Err(e),
                                    Ok(m) => {
                                        if m.is_file() {
                                            Ok(Async::Ready(Some(entry.into_path())))
                                        } else {
                                            Ok(Async::NotReady)
                                        }
                                    }
                                },
                            };
                            match res {
                                Err(e) => return Err(e.into()),
                                Ok(Async::Ready(v)) => return Ok((iter, v)),
                                _ => {}
                            };
                        })
                    })))
                }
                FSBEMachine::Fetching(ref mut fut) => {
                    let (iter, res) = try_ready!(fut.poll());
                    if res.is_none() {
                        let mut it = Some(iter);
                        FSBEMachine::Ending(Box::new(futures::future::poll_fn(move || {
                            blocking::io(|| {
                                it.take();
                                Ok(())
                            })
                        })))
                    } else {
                        self.nextitem = res;
                        FSBEMachine::PrepFetch(Some(iter))
                    }
                }
                FSBEMachine::Ending(ref mut fut) => {
                    try_ready!(fut.poll());
                    return Ok(Async::Ready(None));
                }
            };
            if let Some(path) = self.nextitem.take() {
                let cleaned = path
                    .strip_prefix(self.conf.content_path(""))
                    .expect("Unable to strip known prefix.  Madness ensues!");
                return Ok(Async::Ready(Some(cleaned.to_path_buf())));
            }
        }
    }
}

impl FilesystemBackend {
    pub fn try_new(config: FilesystemBackendConfig) -> Result<FilesystemBackend, io::Error> {
        use std::fs;
        for subpath in ["content", "tmp", "locks"].iter() {
            let mut path = config.base_path.clone();
            path.push(subpath);
            fs::create_dir_all(&path)?;
        }
        Ok(FilesystemBackend {
            conf: Arc::new(config),
        })
    }
}

impl StorageBackendImpl for FilesystemBackend {
    fn get_monotonic_time(&self) -> u64 {
        let nowish = time::SystemTime::now()
            .duration_since(time::UNIX_EPOCH)
            .unwrap();
        (nowish.as_secs() * 1000) + u64::from(nowish.subsec_millis())
    }

    fn check_presence(&self, keys: Vec<PathBuf>) -> Box<StoragePresenceChecker> {
        Box::new(FilesystemBackendPresenceChecker {
            conf: self.conf.clone(),
            in_progress: None,
            tocheck: keys.into_iter().rev().collect(),
            result: Vec::new(),
        })
    }

    fn insert_pair(
        &self,
        key: PathBuf,
        value: Box<StorageValueStream>,
    ) -> Box<StorageInsertionHandler> {
        let handler = FSBInsertionHandler::new(self.conf.clone(), key, value);
        let boxed: Box<StorageInsertionHandler> = Box::new(handler);
        boxed
    }
    fn retrieve_value(&self, key: &Path) -> Box<StorageRetrievalFuture> {
        let retriever = FSBKeyRetriever::new(self.conf.clone(), key.to_path_buf());
        let streamer = retriever
            .then(|res| match res {
                Err(e) => Err(e),
                Ok(Some(fh)) => Ok(fh),
                Ok(None) => Err(io::Error::from(io::ErrorKind::NotFound)),
            })
            .and_then(FilesystemBackendFileStreamer::from_file);
        Box::new(streamer)
    }
    fn remove_pair(&self, key: &Path, min_age: u64) -> Box<StorageRemovalFuture> {
        Box::new(FSBPairRemover {
            conf: self.conf.clone(),
            key: key.to_path_buf(),
            min_age,
            state: FSBRMachine::Start,
            result: None,
            triesleft: self.conf.lock_maxtries,
        })
    }
    fn enumerate_keys(&self, prefix: &Path) -> Box<StorageEnumerationFuture> {
        let stream: Box<StorageEnumerationStream> =
            Box::new(FSBEnumerator::new(self.conf.clone(), prefix));
        Box::new(futures::future::ok(stream))
    }
}

impl Future for FilesystemBackendPresenceChecker {
    type Item = Vec<bool>;
    type Error = io::Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        loop {
            if self.in_progress.is_some() {
                self.result
                    .push(try_ready!(self.in_progress.as_mut().unwrap().poll()));
                self.in_progress = None;
            }
            if let Some(path) = self.tocheck.pop() {
                self.in_progress = Some(FSBIndividualPresenceChecker::new(self.conf.clone(), path))
            } else {
                break;
            }
        }
        use std::mem;
        Ok(Async::Ready(mem::replace(&mut self.result, Vec::new())))
    }
}

mod platform {
    use libc::futimens;
    use std::fs::File;
    use std::io::{Error, Result};
    use std::os::unix::io::AsRawFd;
    use std::ptr::null;

    pub fn set_utime_now(fh: &File) -> Result<()> {
        let fd = fh.as_raw_fd();
        let rc = unsafe { futimens(fd, null()) };
        if rc == 0 {
            Ok(())
        } else {
            Err(Error::last_os_error())
        }
    }
}

mod blocking {
    use futures::{Async::*, Poll};
    use std::io;
    use std::io::ErrorKind::Other;
    use tokio_threadpool;
    pub fn io<F, T>(f: F) -> Poll<T, io::Error>
    where
        F: FnOnce() -> io::Result<T>,
    {
        match tokio_threadpool::blocking(f) {
            Ok(Ready(Ok(v))) => Ok(v.into()),
            Ok(Ready(Err(err))) => Err(err),
            Ok(NotReady) => Ok(NotReady),
            Err(_) => Err(blocking_err()),
        }
    }
    fn blocking_err() -> io::Error {
        io::Error::new(
            Other,
            "`blocking::io` annotated I/O must be called \
             from the context of the Tokio runtime.",
        )
    }

}
