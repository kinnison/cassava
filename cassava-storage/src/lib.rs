#[macro_use]
extern crate futures;
extern crate bytes;
extern crate hex;
extern crate libc;
extern crate sha2;
extern crate tempfile;
extern crate tokio_fs;
extern crate tokio_io;
extern crate tokio_threadpool;
extern crate tokio_timer;
extern crate walkdir;

pub mod backends;
pub mod traits;
mod utils;

pub use crate::traits::*;

/// A value in storage is a sequence of chunks of bytes
pub type StorageChunk = Vec<u8>;

pub use crate::utils::TrivialMemoryStream;

/// The result of attempting to remove something from storage
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum RemovalResult {
    /// The requested pair was too new for removal
    TooNew,
    /// The requested pair was removed
    Removed,
}
