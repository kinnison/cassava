extern crate cassava_storage;
extern crate futures;
extern crate tempdir;
extern crate tokio;
extern crate tokio_timer;

mod generics;

use cassava_storage::{backends, StorageBackend};

fn get_basic_backend(_: std::path::PathBuf) -> Box<StorageBackend> {
    backends::new_memory_backend()
}

fn min_delay() -> u64 {
    0
}
