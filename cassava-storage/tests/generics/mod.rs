// Generics module, no tests in here, instead this is read in with `mod generics;`

use cassava_storage::{RemovalResult, StorageBackend, StorageChunk, TrivialMemoryStream};
use futures;
use futures::future::Future;
use futures::stream::Stream;
use std::io;
use std::path::{Path, PathBuf};
use std::sync::Arc;
use std::time::Duration;
use tempdir::TempDir;
use tokio_timer;

use tokio;

fn gen_delay() -> Box<Future<Item = (), Error = ()> + Send> {
    let delay = crate::min_delay();
    if delay > 0 {
        Box::new(tokio_timer::sleep(Duration::from_micros(delay)).map_err(|_| ()))
    } else {
        Box::new(futures::future::ok(()))
    }
}

#[must_use]
fn look_for<T>(
    storage: &StorageBackend,
    key: T,
) -> impl Future<Item = bool, Error = io::Error> + Send
where
    T: AsRef<Path>,
{
    storage
        .check_presence(vec![key.as_ref().to_owned()])
        .then(|vecres| {
            let checker = vecres.expect("Unable to look up key?");
            assert_eq!(checker.len(), 1);
            Ok(checker[0])
        })
}

#[must_use]
fn insert<T>(
    storage: &StorageBackend,
    key: T,
    values: Vec<StorageChunk>,
    commit: bool,
) -> impl Future<Item = (), Error = io::Error> + Send
where
    T: AsRef<Path>,
{
    storage
        .insert_pair(
            key.as_ref().to_owned(),
            TrivialMemoryStream::from_chunks(values),
        )
        .then(move |res| {
            let inserter = res.expect("Unable to prepare inserter");
            if commit {
                inserter.commit()
            } else {
                inserter.abort()
            }
        })
}

#[must_use]
fn retrieve<T>(
    storage: &StorageBackend,
    key: T,
) -> impl Future<Item = Vec<StorageChunk>, Error = io::Error>
where
    T: AsRef<Path>,
{
    storage
        .retrieve_value(key.as_ref())
        .then(|retriever| retriever.expect("Unable to build a retriever?").collect())
}

#[must_use]
fn remove<T>(
    storage: &StorageBackend,
    key: T,
    age: u64,
) -> impl Future<Item = RemovalResult, Error = io::Error>
where
    T: AsRef<Path>,
{
    storage.remove_pair(key.as_ref(), age)
}

fn pbuf<T>(p: T) -> PathBuf
where
    T: AsRef<Path>,
{
    let mut ret = PathBuf::new();
    ret.push(p);
    ret
}

fn simple_sequence(storage: Box<StorageBackend>) {
    let starttime = storage.get_monotonic_time();
    let storage: Arc<Box<StorageBackend>> = Arc::new(storage);
    let mut rt = tokio::runtime::Runtime::new().expect("Unable to construct a Tokio runtime!");
    let result: Result<bool, io::Error> = rt.block_on(
        // 1. Ensure that the key we're about to insert isn't there
        look_for(&**storage, "simple/key")
            .then({
                let storage = storage.clone();
                move |res| {
                    assert_eq!(res.expect("Unable to lookup key"), false);
                    // 2. Insert something for our simple key.
                    gen_delay().then(move |_| {
                        insert(
                            &**storage,
                            "simple/key",
                            vec![
                                "first".to_owned().into_bytes(),
                                "second".to_owned().into_bytes(),
                                "third".to_owned().into_bytes(),
                            ],
                            true,
                        )
                    })
                }
            })
            .then({
                let storage = storage.clone();
                move |res| {
                    // 3. Ensure the key is now present
                    res.expect("Unable to insert key");
                    gen_delay().then(move |_| look_for(&**storage, "simple/key"))
                }
            })
            .then({
                let storage = storage.clone();
                move |res| {
                    assert_eq!(res.expect("Unable to lookup key?"), true);
                    // 4. Abortively don't add another key
                    gen_delay().then(move |_| look_for(&**storage, "another_key"))
                }
            })
            .then({
                let storage = storage.clone();
                move |res| {
                    assert_eq!(res.expect("Unable to lookup key?"), false);
                    gen_delay().then(move |_| insert(&**storage, "another_key", vec![], false))
                }
            })
            .then({
                let storage = storage.clone();
                move |res| {
                    res.expect("Unable to insert another_key");
                    gen_delay().then(move |_| look_for(&**storage, "another_key"))
                }
            })
            .then({
                let storage = storage.clone();
                move |res| {
                    assert_eq!(res.expect("Unable to lookup key?"), false);
                    // 5. Ensure that the simple key was stored properly
                    gen_delay().then(move |_| retrieve(&**storage, "simple/key"))
                }
            })
            .then({
                let storage = storage.clone();
                move |content| {
                    let content = content.expect("Unable to retrieve content of simple/key");
                    let content: String =
                        String::from_utf8(content.into_iter().flat_map(|v| v).collect()).unwrap();
                    assert_eq!(content, "firstsecondthird");
                    let endtime = storage.get_monotonic_time();
                    assert!(endtime >= starttime);
                    Ok(endtime)
                }
            })
            .then({
                let storage = storage.clone();
                move |endtime: Result<u64, io::Error>| {
                    // 6. Enumerate the keys
                    gen_delay().then(move |_| {
                        storage.enumerate_keys("simple".as_ref()).then(move |res| {
                            res.expect("Unable to construct enumeration")
                                .collect()
                                .then({
                                    let storage = storage.clone();
                                    move |keys| {
                                        assert_eq!(
                                            keys.expect("Enumeration itself failed"),
                                            vec![pbuf("simple/key")]
                                        );
                                        // 7. Delete the simple key
                                        gen_delay().then(move |_| {
                                            remove(&**storage, "simple/key", starttime)
                                        })
                                    }
                                })
                                .then({
                                    let storage = storage.clone();
                                    move |res| {
                                        assert_eq!(
                                            res.expect("Unable to request removal"),
                                            RemovalResult::TooNew
                                        );
                                        gen_delay().then(move |_| {
                                            remove(&**storage, "simple/key", endtime.unwrap())
                                        })
                                    }
                                })
                                .then({
                                    let storage = storage.clone();
                                    move |res| {
                                        assert_eq!(
                                            res.expect("Unable to request removal"),
                                            RemovalResult::Removed
                                        );
                                        // 8. Fail to delete the unknown key
                                        gen_delay()
                                            .then(move |_| remove(&**storage, "another_key", 0))
                                    }
                                })
                                .then(|res| {
                                    assert!(res.is_err());
                                    assert_eq!(res.err().unwrap().kind(), io::ErrorKind::NotFound);
                                    Ok(true)
                                })
                        })
                    })
                }
            }),
    );
    rt.shutdown_now().wait().unwrap();
    assert!(result.expect("Unable to run future to completion!"));
}

#[test]
fn test_basic_sequence() {
    let tempdir =
        TempDir::new("cassava-fs-backend-test").expect("Unable to prepare temporary directory");
    simple_sequence(crate::get_basic_backend(tempdir.path().into()));
}
