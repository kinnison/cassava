extern crate cassava_storage;
extern crate futures;
extern crate tempdir;
extern crate tokio;
extern crate tokio_timer;

mod generics;

use cassava_storage::{backends, StorageBackend};

fn get_basic_backend(tempdir: std::path::PathBuf) -> Box<StorageBackend> {
    backends::new_filesystem_backend(backends::FilesystemBackendConfig {
        base_path: tempdir,
        max_unlocked: 20,
        min_removal: 40,
        max_locked: 10,
        lock_maxtries: 5,
        lock_trygap: 1,
    })
    .unwrap()
}

fn min_delay() -> u64 {
    match std::env::var("GO_SLOW") {
        Ok(n) => n.parse().unwrap(),
        _ => 5_000,
    }
}
